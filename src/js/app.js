import React, { Component } from "react";
import { render } from "react-dom";
import AddTask from "../components/addTask";
import DeleteTask from "../components/deleteTask";
import '../css/style.css';

class Todo extends Component {
  constructor() {
    super();
    this.state = {
      todoTasks: []
    }
    this.onAdd = this.onAdd.bind(this);
    this.onDelete = this.onDelete.bind(this);
    this.onChecked = this.onChecked.bind(this);
  }

  getTask() {
    return this.state.todoTasks;
  }

  onAdd(task) {
    const todoTasks = this.getTask();
    if (!todoTasks.includes(task)) todoTasks.push(task);
    this.setState(todoTasks);
  }


  onDelete(taskId) {
    const todoTasks = this.getTask();
    const filterTask = todoTasks.filter(task => {
      return task !== taskId;
    });
    this.setState({ todoTasks: filterTask });
  }

  onChecked(taskId,event) {
    const todoTasks = this.getTask();
    const filterTask = todoTasks.filter(task => {
      let state = event.target.checked === true ? "checked": "unchecked";
      if(task === taskId){
        return event.target.parentNode.nextSibling.childNodes[0].className=state;
      }else{
        return task;
      }
    });
    this.setState({ todoTasks: filterTask });
    console.log(event.target.value)
  }

  render() {
    return (
      <div>
        <h1>TODO APPLICATION</h1>
        <AddTask onAdd={this.onAdd} />
        {this.state.todoTasks.map(task => {
          return <DeleteTask key={task} task={task} onDelete={this.onDelete}  onChecked={this.onChecked}/>;
        })}
      </div>
    );
  }
}

render(<Todo />, document.getElementById("app"));
export default Todo;
