import React, { Component } from "react";

class DeleteTask extends Component {
  render() {
    const task = this.props.task;
    return (
      <div className="container">
        <div className="item">
        <input className={'checked'} 
          onChange={event => this.props.onChecked(this.props.task, event)}
          type="checkbox"
          id={task}
          
        />
        </div>
        <div  className="item">
          <span id={task}>{task}</span>
        </div>
        <div  className="item">
          <button
            onClick={() => this.props.onDelete(this.props.task)}
            className="deleteBtn"
          >
            Delete
          </button>
        </div>
      </div>
    );
  }
}

export default DeleteTask;
