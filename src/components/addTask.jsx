import React, { Component } from 'react';

class AddTask extends Component {
    
    constructor(){
        super();
        this.onSubmit=this.onSubmit.bind(this);    
    }

    onSubmit(event){
        event.preventDefault();
        this.props.onAdd(this.inputTask.value);
        this.inputTask.value='';
    }

    render() { 
        return ( 
        <form onSubmit={this.onSubmit}>
            <h3>Add Task</h3>
            <input className="inputField" placeholder="Enter the task...." ref={inputTask=>this.inputTask=inputTask}/>
            <button className="addBtn">ADD</button>
            <hr/>
        </form> );
    }
}
 
export default AddTask;
